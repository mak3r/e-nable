use <bezier.scad>

union() {
*	smooth_F();
*	smooth_L();
*	smooth_R();
}
//BezCubicRibbon([[0,15],[6,13],[3,5],[15,0]], [[0,13],[6,11],[3,2],[13,0]]);
	

module smooth_F() {
	translate([-55,89,0])
	rotate(90,[0,0,1])
	rotate(90,[1,0,0])
	BezCubicRibbon([[0,11],[4,8],[0,0],[15,2]], [[0,15],[9,11],[14,9],[15,3]], height=110);
}

module smooth_L() {
	translate([-40,0,0])
	rotate(180,[0,0,1])
	rotate(90,[1,0,0])
	BezCubicRibbon([[0,11],[4,8],[0,0],[15,2]], [[0,15],[9,11],[14,9],[15,3]], height=110, scale=.85);
}
	BezCubicRibbon([[0,11],[4,8],[0,0],[15,2]], [[0,15],[9,11],[14,9],[15,3]], height=110, scale=.85);

module smooth_R() {
	translate([39,0,0])
	rotate(180,[0,0,1])
	rotate(90,[1,0,0])
	mirror([1,0,0])
	BezCubicRibbon([[0,11],[4,8],[0,0],[15,2]], [[0,15],[9,11],[14,9],[15,3]], height=110, scale=1);
}
