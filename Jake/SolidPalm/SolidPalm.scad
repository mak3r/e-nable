
use <bezier.scad>

UNDER_CONSTRUCTION = 0;
BASE_MODEL = 12;
MID_MODEL = 15;
DEEP_MODEL = 19;

gModel = DEEP_MODEL;
gUseSupports = 1;
gPlateHeight = 4;

solid_palm();

module solid_palm(useSupports = gUseSupports, model = gModel) {
	difference() {
		union() {
			difference() {
				mountable_palm(model);

				if(UNDER_CONSTRUCTION == 0) {	
					smooth_L(model);
					smooth_R(model);
					smooth_F(model);
				}
			}


			if (useSupports == 1)
				supports();
		}
		base_clip();			
		screw_cap_slots();
	}
}


module screw_cap_slots() {
	scsHeight = gPlateHeight - 2;
	//top view lower right
	translate([46,18.25,scsHeight])
	cylinder(d=6,h=12,$fn=15);

	//top view upper right
	translate([44.75,88.5,scsHeight])
	cylinder(d=6,h=12,$fn=15);

	//top view lower left
	translate([-46.125,18.25,scsHeight])
	cylinder(d=6,h=12,$fn=15);

	//top view upper left
	translate([-45,88.5,scsHeight])
	cylinder(d=6,h=12,$fn=15);
}

module base_clip() {
	translate([-55,0,-4.8])
	cube([110,110,5]);
}

module mountable_palm(model = gModel) {
	difference() {
		union() {
			translate([0,0,1.5])
			palm_base(thickness=model);
			join_plate(height=gPlateHeight);
		}
		wrist_clip(model);
	}

}

module smooth_F(height=gModel) {
	offset = (height-BASE_MODEL)/2;

	translate([-55,82,0])
	rotate(90,[0,0,1])
	rotate(90,[1,0,0])
	BezCubicRibbon([[0,6+offset],[4,8],[0,1],[20,2]], [[0,15+offset],[9,11],[14,9],[20,3]], height=110);
}

module smooth_L(height=gModel) {
	offset = (height-BASE_MODEL)/2;

	translate([-39,0,0])
	rotate(180,[0,0,1])
	rotate(90,[1,0,0])	
	BezCubicRibbon([[0,(11+offset)],[4,8],[0,0],[16,2]], [[0,15+offset],[9,11],[14,9],[16,3]], height=110);
}

module smooth_R(height=gModel) {
	offset = (height-BASE_MODEL)/2;

	translate([39,0,0])
	rotate(180,[0,0,1])
	rotate(90,[1,0,0])
	mirror([1,0,0])
	BezCubicRibbon([[0,11+offset],[4,8],[0,0],[15,2]], [[0,15+offset],[9,11],[14,9],[15,3]], height=110);
}

module wrist_clip(height=gModel) {
	translate([-80,-23,-12])
	cube([160,30,70]);

	translate([-80,2,6+(height-BASE_MODEL)])
	rotate(37,[1,0,0])
	cube([160,10,5]);
}


module join_plate(height) {
	difference() {
		linear_extrude(height=height) 
		attachment();
		knuckle_clip(height=2*height);
	}
	
	module knuckle_clip(height) {
		scale([1,.66,1])
		translate([0,0,-1])
		difference() {
			cylinder(r=175, h=height);
			translate([0,0,-1])
			cylinder(r=152, h=height+2);
		}
	}

	module attachment() {
		projection(cut=true)
		difference() {
			translate([0,0,16.5])
			mirror([1,0,0])
			import("Left.Raptor-Palm-1_fixed.stl");

			translate([-65.5,33,-4])
			rotate(-12,[0,0,1])
			cube([14,16,10]);
		
		}
	}

}

module supports(height=gModel) {
	translate([0,0,0])
	difference() {	
		if (height == DEEP_MODEL) {
			scale([1,1.01,1.1])	
			wrist_part();
		} else if (height == MID_MODEL) {
			scale([1,1.02,1.125])	
			wrist_part();
		} else {
			scale([1,1.03,1.15])	
			wrist_part();
		}
						
		translate([-60,-2,-80])
		cube([120,120,80]);
		
		translate([-60,-113,-60])
		cube([120,120,80]);
		
		translate([18,-2,-1])
		cube([30,120,60]);

		translate([1,-2,-1])
		cube([16,120,60]);

		translate([-16,-2,-1])
		cube([16,120,60]);

		translate([-42,-2,-1])
		cube([25,120,60]);
	}
}


///////////////////////////////////////////////////////////////////////////////
/// SECTION - palm_base()
///////////////////////////////////////////////////////////////////////////////
//palm_base();
//Some tests of the sub areas of palm_base
//flipped_palm();
//four_cubes();

module palm_base(thickness=gModel) {
	if (thickness < gModel)
		echo("WARNING - Unexpected small thickness value!!", thickness);
	echo("palm_base.thickness:", thickness );
	difference() {
		union() {
			difference() {
				hull() 
				resize([0,0,thickness])
				flipped_palm();
			
				center_palm(height=thickness);
			}
				
			thumb_palm(height=thickness);
			pinky_palm(height=thickness);
		}
	
		wrist_part(height=thickness);
	}
}

module four_cubes() {
	//fingers
	translate([-80,92,-12])
	cube([160,35,50]);

	//wrist	
	translate([-80,-23,-12])
	cube([160,30,70]);
	
	//-x
	translate([-53.5,-17,-12])
	cube([10,160,40]);

	//+x
	translate([43,-17,-12])
	cube([12,160,40]);
}

module flipped_palm() {
	translate([0,0,10])
	difference() {
		difference() {
			mirror([1,0,0])
			import("Left.Raptor-Palm-1_fixed.stl");

			translate([-80,25,-25])
			cube([30,40,40]);
		}
		translate([0,0,-8])
		four_cubes();
	}
}

module wrist_part(height=gModel) {
	if (height == DEEP_MODEL) {
		translate([0,0,-4])
		rotate(90,[1,0,0])
		scale([.7506,.3587,1])
		sphere(d=100);
	} else if (height == MID_MODEL) {
			translate([0,0,-4])
		//resize([65,65,0])
		rotate(90,[1,0,0])
		scale([.7506,.3043,1])
		sphere(d=100);
	} else {
		translate([0,0,-4])
		rotate(90,[1,0,0])
		scale([.7506,.2587,1])
		sphere(d=100);
	}
}

module thumb_palm(height=gModel) {
		translate([-18,29,height-(height/2)+1])
		rotate(-5,[1,0,0])
		rotate(-39,[0,0,1])
		scale([2.3,1.2,.7])
		sphere(12);
}

module pinky_palm(height=gModel) {
	translate([22,29,height-(height/3)])
	rotate(-10,[1,0,0])
	rotate(45,[0,0,1])
	scale([2,1.2,.4])
	sphere(12);
}

module center_palm(height=gModel) {
	ht = 36 + ((height-BASE_MODEL)/3);
	ymv = 65 + ((height-BASE_MODEL)*3);
	rotate(-6.5,[1,0,0])
	translate([0,ymv,ht])
	scale([.8,1,.24])
	sphere(100);
}
///////////////////////////////////////////////////////////////////////////////
/// END SECTION - palm_base()
///////////////////////////////////////////////////////////////////////////////
